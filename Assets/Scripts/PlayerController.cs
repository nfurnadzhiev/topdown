﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 10.0f;
    Animator m_Animator;
    bool facingRight = true;

    void Start()
    {
        m_Animator = gameObject.GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        var rigidbody2D = GetComponent<Rigidbody2D>();

        float move = Input.GetAxis("Horizontal");
        float move2 = Input.GetAxis("Vertical");
        rigidbody2D.velocity = new Vector2(move * speed, move2 * speed);

        limitBoard();

        if (move > 0 && !facingRight)
        {
            Flip();
        }

        else if (move < 0 && facingRight)
        {
            Flip();
        }

        if (Input.GetKey(KeyCode.J))
        {
            m_Animator.SetTrigger("playerAttack");
        }

        if (Input.GetKey(KeyCode.K))
        {
            m_Animator.SetTrigger("playerAttack2");
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector2 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    
    void limitBoard()
    {
        // X axis
        if (transform.position.x <= -10.0f)
        {
            transform.position = new Vector2(-10.0f, transform.position.y);
        }
        else if (transform.position.x >= 10.0f)
        {
            transform.position = new Vector2(10.0f, transform.position.y);
        }

        // Y axis
        if (transform.position.y <= -5.0f)
        {
            transform.position = new Vector2(transform.position.x, -5.0f);
        }
        else if (transform.position.y >= 5.0f)
        {
            transform.position = new Vector2(transform.position.x, 5.0f);
        }
    }
}